<?php

namespace Viewflex\BlogExample;


use Illuminate\Support\ServiceProvider;

class BlogExampleServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $resource_namespace = 'blog-example';

        if ($this->app->runningInConsole()) {
            $this->commands([
                //
            ]);
        }

        /*
    	|--------------------------------------------------------------------------
    	| Set the Default Internal Namespace for Translations and Views
    	|--------------------------------------------------------------------------
    	*/

        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang', $resource_namespace);
        $this->loadViewsFrom(__DIR__ . '/Resources/views', $resource_namespace);

        /*
        |--------------------------------------------------------------------------
        | Publish Migration and Seeder
        |--------------------------------------------------------------------------
        */

        $this->publishes([
            __DIR__ . '/Database/Migrations' => base_path('database/migrations'),
            __DIR__ . '/Database/Seeds' => base_path('database/seeds')
        ], 'blog-example');


        $this->loadRoutesFrom(__DIR__ . '/Publish/routes.php');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
