<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogComments;


use Viewflex\Ligero\Base\BasePublisherController;

class BlogCommentsController extends BasePublisherController
{

    public function __construct()
    {

        $this->createPublisherWithDefaults();

        $this
            ->setDomain('BlogComments')
            ->setResourceNamespace('blog-example')
            ->setTranslationFile('blog-domains')
            ->setTableName('blog_comments')
            ->setModelName('Viewflex\BlogExample\Publish\Demo\BlogComments\BlogComment')
            ->setResultsColumns([
                'id',
                'post_id',
                'name',
                'email',
                'body'
            ])
            ->setWildcardColumns([
                'name',
                'email'
            ])
            ->setControls([
                'pagination'        => true,
                'keyword_search'    => true
            ])
            ->setKeywordSearchColumns([
                'name',
                'email'
            ])
            ->setQueryRules([
                'id'                => 'numeric|min:1',
                'post_id'           => 'numeric|min:1',
                'name'              => 'max:120',
                'email'             => 'max:120',
            ])
            ->setRequestRules([
                'id'                => 'numeric|min:1',
                'post_id'           => 'numeric|min:1',
                'name'              => 'max:120',
                'email'             => 'max:120',
                'body'              => 'max:250'
            ]);

    }

}
