<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogComments;


use Viewflex\Ligero\Base\BaseModel;

class BlogComment extends BaseModel {
    
    protected $table = 'blog_comments';

    /**
     * The Presenter class for generating formatted content from this model.
     *
     * @var string
     */
    protected $presenter = 'Viewflex\BlogExample\Publish\Demo\BlogComments\BlogCommentPresenter';
    
}
