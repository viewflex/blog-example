<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogComments;


use Viewflex\Ligero\Base\BasePresenter;
use Viewflex\Ligero\Exceptions\PresenterException;

class BlogCommentPresenter extends BasePresenter
{
    /**
     * Returns an array of dynamic fields for current item.
     *
     * @return array
     * @throws PresenterException
     */
    public function dynamicFields()
    {
        $data = [];
        $this->requireConfig();


        return $data;
    }

}
