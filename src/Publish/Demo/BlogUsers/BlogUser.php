<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogUsers;


use Viewflex\Ligero\Base\BaseModel;

class BlogUser extends BaseModel {
    
    protected $table = 'blog_users';

    /**
     * The Presenter class for generating formatted content from this model.
     *
     * @var string
     */
    protected $presenter = 'Viewflex\BlogExample\Publish\Demo\BlogUsers\BlogUserPresenter';
    
}
