<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogUsers;


use Viewflex\Ligero\Base\BasePublisherController;

class BlogUsersController extends BasePublisherController
{

    public function __construct()
    {

        $this->createPublisherWithDefaults();

        $this
            ->setDomain('BlogUsers')
            ->setResourceNamespace('blog-example')
            ->setTranslationFile('blog-domains')
            ->setTableName('blog_users')
            ->setModelName('Viewflex\BlogExample\Publish\Demo\BlogUsers\BlogUser')
            ->setResultsColumns([
                'id',
                'name',
                'username',
                'email',
                'phone',
                'website'
            ])
            ->setWildcardColumns([
                'name',
                'username',
                'email'
            ])
            ->setControls([
                'pagination'        => true,
                'keyword_search'    => true
            ])
            ->setKeywordSearchColumns([
                'name',
                'username',
                'email'
            ])
            ->setQueryRules([
                'id'                => 'numeric|min:1',
                'name'              => 'max:120',
                'username'          => 'max:120',
                'email'             => 'max:120',
                'phone'             => 'max:32',
                'website'           => 'max:120'
            ])
            ->setRequestRules([
                'id'                => 'numeric|min:1',
                'name'              => 'max:120',
                'username'          => 'max:120',
                'email'             => 'max:120',
                'phone'             => 'max:32',
                'website'           => 'max:120'
            ]);

    }

}
