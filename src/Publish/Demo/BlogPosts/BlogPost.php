<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogPosts;


use Viewflex\Ligero\Base\BaseModel;

class BlogPost extends BaseModel {
    
    protected $table = 'blog_posts';

    /**
     * The Presenter class for generating formatted content from this model.
     *
     * @var string
     */
    protected $presenter = 'Viewflex\BlogExample\Publish\Demo\BlogPosts\BlogPostPresenter';
    
}
