<?php

namespace Viewflex\BlogExample\Publish\Demo\BlogPosts;


use Viewflex\Ligero\Base\BasePublisherController;

class BlogPostsController extends BasePublisherController
{

    public function __construct()
    {

        $this->createPublisherWithDefaults();

        $this
            ->setDomain('BlogPosts')
            ->setResourceNamespace('blog-example')
            ->setTranslationFile('blog-domains')
            ->setTableName('blog_posts')
            ->setModelName('Viewflex\BlogExample\Publish\Demo\BlogPosts\BlogPost')
            ->setResultsColumns([
                'id',
                'user_id',
                'title',
                'body'
            ])
            ->setWildcardColumns([
                'title',
            ])
            ->setControls([
                'pagination'        => true,
                'keyword_search'    => true
            ])
            ->setKeywordSearchColumns([
                'title',
            ])
            ->setQueryRules([
                'id'                => 'numeric|min:1',
                'user_id'           => 'numeric|min:1',
                'title'             => 'max:120',
            ])
            ->setRequestRules([
                'id'                => 'numeric|min:1',
                'user_id'           => 'numeric|min:1',
                'title'             => 'max:120',
                'body'              => 'max:250'
            ]);

    }

}
