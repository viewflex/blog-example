<?php

// BlogExample endpoints:

Route::get('blog/users/json', array('as' => 'blog.users.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogUsers\BlogUsersController@json', 'middleware' => 'api'));
Route::get('blog/posts/json', array('as' => 'blog.posts.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogPosts\BlogPostsController@json', 'middleware' => 'api'));
Route::get('blog/comments/json', array('as' => 'blog.comments.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogComments\BlogCommentsController@json', 'middleware' => 'api'));
