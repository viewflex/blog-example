<?php

namespace Viewflex\BlogExample\Database\Schema;


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlogExampleSchema
{
    /**
     * Run the migrations to create BlogExample schema.
     * 
     * @param array $tables
     */
    public static function create($tables = [])
    {
        // ----------------------------------------
        // Blog Users
        // ----------------------------------------

        Schema::create($tables['blog_users'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 120)->nullable();
            $table->string('username', 120)->nullable();
            $table->string('email', 120)->nullable();
            $table->string('phone', 32)->nullable();
            $table->string('website', 120)->nullable();

            $table->index('name');
            $table->index('username');
            $table->index('email');
        });


        // ----------------------------------------
        // Blog Posts
        // ----------------------------------------

        Schema::create($tables['blog_posts'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title', 120)->nullable();
            $table->text('body')->nullable();

            $table->index('title');

            $table->foreign('user_id')
                ->references('id')
                ->on($tables['blog_users'] ?? 'blog_users')
                ->onDelete('cascade');

        });


        // ----------------------------------------
        // Blog Comments
        // ----------------------------------------

        Schema::create($tables['blog_comments'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            $table->string('name', 120)->nullable();
            $table->string('email', 120)->nullable();
            $table->text('body')->nullable();

            $table->index('name');
            $table->index('email');

            $table->foreign('post_id')
                ->references('id')
                ->on($tables['blog_posts'] ?? 'blog_posts')
                ->onDelete('cascade');

        });

    }
    
    /**
     * Reverse the migration.
     * 
     * @param array $tables
     */
    public static function drop($tables = [])
    {
        Schema::drop($tables['blog_comments']);
        Schema::drop($tables['blog_posts']);
        Schema::drop($tables['blog_users']);
    }
    
    
}
