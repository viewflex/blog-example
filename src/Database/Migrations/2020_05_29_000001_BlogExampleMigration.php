<?php


use Illuminate\Database\Migrations\Migration;
use Viewflex\BlogExample\Database\Schema\BlogExampleSchema;

/**
 * Creates the database tables for the Viewflex/BlogExample package.
 */
class BlogExampleMigration extends Migration
{
    protected $tables = [
        'blog_users'        => 'blog_users',
        'blog_posts'        => 'blog_posts',
        'blog_comments'     => 'blog_comments',
    ];
    
    /**
     * Run the migrations.
     */
    public function up()
    {
        BlogExampleSchema::create($this->tables);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        BlogExampleSchema::drop($this->tables);
    }
}
