# Blog Example

Demonstrating an example of API and scaffolding for a Laravel blog application, with users, posts and comments. Implemented using the [`viewflex/ligero`](https://github.com/viewflex/ligero) BREAD framework.

## System Requirements

Laravel framework version 5.2 or higher.

## Installation

In your application's `composer.json` file, add the values displayed below:

```json
{
    "require": {
        "viewflex/blog-example": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:viewflex/blog-example.git"
        }
    ]
}
```
Then run this command to install the package:

```bash
composer update
```

### Register the Service Provider

After installing, add the `BlogExampleServiceProvider` to the list of providers in `config/app.php`.

```php
Viewflex\BlogExample\BlogExampleServiceProvider::class,
```

### Publish Migration and Seeder

Run this command to install the migration and seeder for the 'BlogExample' demo domains:

```bash
php artisan vendor:publish  --tag='blog-example'
```

After publishing the demo migration and seeder, run the migration:

```bash
php artisan migrate
```

Then run the seeder:

```bash
php artisan db:seed --class="BlogExampleSeeder"
```

## Routing

These routes provide paginated json responses to GET queries on the tables:

```php
Route::get('blog/users/json', array('as' => 'blog.users.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogUsers\BlogUsersController@json', 'middleware' => 'api'));
Route::get('blog/posts/json', array('as' => 'blog.posts.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogPosts\BlogPostsController@json', 'middleware' => 'api'));
Route::get('blog/comments/json', array('as' => 'blog.comments.json', 'uses' => '\Viewflex\BlogExample\Publish\Demo\BlogComments\BlogCommentsController@json', 'middleware' => 'api'));

```

## Usage Examples

The endpoints return a success response in the form of a json object containing the requested results set, plus pagination and keyword search controls, configuration, defaults and inputs (failed queries currently return just an empty response with 200 status).

Specific query parameters are supported for id, parent id, and other table columns, and for pagination via start row or page number. Please refer to the [Ligero](https://github.com/viewflex/ligero) documentation, which explains the architecture and many configuration options of this BREAD framework - only a tiny portion of the functionality is deployed for this demo.

#### Get Users with email containing given string.

##### Request

```text
/blog/users/json?email=biz
```

##### Response

```json
{
    "query": {
        "self": "http://fresh58.srv/blog/users/json?email=biz",
        "query": "?email=biz",
        "route": "/blog/users/json",
        "parameters": {
            "inputs": {
                "email": "biz"
            },
            "defaults": {
                "view": "list"
            },
            "all": {
                "email": "biz",
                "view": "list"
            }
        },
        "view": "list",
        "limit": 5,
        "start": 0,
        "found": 3,
        "displayed": 3
    },
    "items": [
        {
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "phone": "1-770-736-8031 x56442",
            "website": "hildegard.org",
            "list_view": "#",
            "grid_view": "/blog/users/json?email=biz&view=grid&start=0",
            "item_view": "/blog/users/json?email=biz&view=item&start=0",
            "default_view": "/blog/users/json?email=biz&start=0",
            "display_position": 1,
            "query_position": 0,
            "form_item_checked": ""
        },
        {
            "id": 7,
            "name": "Kurtis Weissnat",
            "username": "Elwyn.Skiles",
            "email": "Telly.Hoeger@billy.biz",
            "phone": "210.067.6132",
            "website": "elvis.io",
            "list_view": "#",
            "grid_view": "/blog/users/json?email=biz&view=grid&start=1",
            "item_view": "/blog/users/json?email=biz&view=item&start=1",
            "default_view": "/blog/users/json?email=biz&start=1",
            "display_position": 2,
            "query_position": 1,
            "form_item_checked": ""
        },
        {
            "id": 10,
            "name": "Clementina DuBuque",
            "username": "Moriah.Stanton",
            "email": "Rey.Padberg@karina.biz",
            "phone": "024-648-3804",
            "website": "ambrose.net",
            "list_view": "#",
            "grid_view": "/blog/users/json?email=biz&view=grid&start=2",
            "item_view": "/blog/users/json?email=biz&view=item&start=2",
            "default_view": "/blog/users/json?email=biz&start=2",
            "display_position": 3,
            "query_position": 2,
            "form_item_checked": ""
        }
    ],
    "pagination": {
        "config": {
            "pager": {
                "make": true,
                "context": "logical"
            },
            "page_menu": {
                "make": true,
                "context": "logical",
                "max_links": 5
            },
            "view_menu": {
                "make": true,
                "context": "logical"
            },
            "use_page_number": true
        },
        "route": "/blog/users/json",
        "viewing": {
            "view": "list",
            "first": "1",
            "last": "3",
            "range": "1~3",
            "found": 3,
            "displayed": 3
        },
        "pager": {
            "context": "logical",
            "pages": {
                "first": null,
                "prev": null,
                "next": null,
                "last": null
            },
            "page_num": 1,
            "num_pages": 1
        },
        "page_menu": {
            "context": "logical",
            "pages": {
                "1": {
                    "start": 0,
                    "url": null
                }
            },
            "page_num": 1,
            "num_pages": 1
        },
        "view_menu": {
            "context": "logical",
            "views": {
                "list": {
                    "display": "List",
                    "limit": 5,
                    "url": null
                },
                "grid": {
                    "display": "Grid",
                    "limit": 20,
                    "url": "/blog/users/json?email=biz&view=grid"
                },
                "item": {
                    "display": "Item",
                    "limit": 1,
                    "url": "/blog/users/json?email=biz&view=item"
                }
            },
            "selected": "list",
            "label_view_as": "View as"
        }
    },
    "keyword_search": {
        "config": {
            "scope": "query",
            "persist_sort": true,
            "persist_view": true,
            "persist_input": false
        },
        "route": "/blog/users/json",
        "base_parameters": {
            "email": "biz"
        },
        "keyword": "",
        "clear": "/blog/users/json?email=biz",
        "label_search": "Search"
    }
}
```

#### Get Posts by User ID

##### Request

```text
/blog/posts/json?user_id=2
```

```json
{
    "query": {
        "self": "http://fresh58.srv/blog/posts/json?user_id=2",
        "query": "?user_id=2",
        "route": "/blog/posts/json",
        "parameters": {
            "inputs": {
                "user_id": "2"
            },
            "defaults": {
                "view": "list"
            },
            "all": {
                "user_id": "2",
                "view": "list"
            }
        },
        "view": "list",
        "limit": 5,
        "start": 0,
        "found": 10,
        "displayed": 5
    },
    "items": [
        {
            "id": 11,
            "user_id": 2,
            "title": "et ea vero quia laudantium autem",
            "body": "delectus reiciendis molestiae occaecati non minima eveniet qui voluptatibus\naccusamus in eum beatae sit\nvel qui neque voluptates ut commodi qui incidunt\nut animi commodi",
            "list_view": "#",
            "grid_view": "/blog/posts/json?user_id=2&view=grid&start=0",
            "item_view": "/blog/posts/json?user_id=2&view=item&start=0",
            "default_view": "/blog/posts/json?user_id=2&start=0",
            "display_position": 1,
            "query_position": 0,
            "form_item_checked": ""
        },
        {
            "id": 12,
            "user_id": 2,
            "title": "in quibusdam tempore odit est dolorem",
            "body": "itaque id aut magnam\npraesentium quia et ea odit et ea voluptas et\nsapiente quia nihil amet occaecati quia id voluptatem\nincidunt ea est distinctio odio",
            "list_view": "#",
            "grid_view": "/blog/posts/json?user_id=2&view=grid&start=1",
            "item_view": "/blog/posts/json?user_id=2&view=item&start=1",
            "default_view": "/blog/posts/json?user_id=2&start=1",
            "display_position": 2,
            "query_position": 1,
            "form_item_checked": ""
        },
        {
            "id": 13,
            "user_id": 2,
            "title": "dolorum ut in voluptas mollitia et saepe quo animi",
            "body": "aut dicta possimus sint mollitia voluptas commodi quo doloremque\niste corrupti reiciendis voluptatem eius rerum\nsit cumque quod eligendi laborum minima\nperferendis recusandae assumenda consectetur porro architecto ipsum ipsam",
            "list_view": "#",
            "grid_view": "/blog/posts/json?user_id=2&view=grid&start=2",
            "item_view": "/blog/posts/json?user_id=2&view=item&start=2",
            "default_view": "/blog/posts/json?user_id=2&start=2",
            "display_position": 3,
            "query_position": 2,
            "form_item_checked": ""
        },
        {
            "id": 14,
            "user_id": 2,
            "title": "voluptatem eligendi optio",
            "body": "fuga et accusamus dolorum perferendis illo voluptas\nnon doloremque neque facere\nad qui dolorum molestiae beatae\nsed aut voluptas totam sit illum",
            "list_view": "#",
            "grid_view": "/blog/posts/json?user_id=2&view=grid&start=3",
            "item_view": "/blog/posts/json?user_id=2&view=item&start=3",
            "default_view": "/blog/posts/json?user_id=2&start=3",
            "display_position": 4,
            "query_position": 3,
            "form_item_checked": ""
        },
        {
            "id": 15,
            "user_id": 2,
            "title": "eveniet quod temporibus",
            "body": "reprehenderit quos placeat\nvelit minima officia dolores impedit repudiandae molestiae nam\nvoluptas recusandae quis delectus\nofficiis harum fugiat vitae",
            "list_view": "#",
            "grid_view": "/blog/posts/json?user_id=2&view=grid&start=4",
            "item_view": "/blog/posts/json?user_id=2&view=item&start=4",
            "default_view": "/blog/posts/json?user_id=2&start=4",
            "display_position": 5,
            "query_position": 4,
            "form_item_checked": ""
        }
    ],
    "pagination": {
        "config": {
            "pager": {
                "make": true,
                "context": "logical"
            },
            "page_menu": {
                "make": true,
                "context": "logical",
                "max_links": 5
            },
            "view_menu": {
                "make": true,
                "context": "logical"
            },
            "use_page_number": true
        },
        "route": "/blog/posts/json",
        "viewing": {
            "view": "list",
            "first": "1",
            "last": "5",
            "range": "1~5",
            "found": 10,
            "displayed": 5
        },
        "pager": {
            "context": "logical",
            "pages": {
                "first": null,
                "prev": null,
                "next": "/blog/posts/json?user_id=2&page=2",
                "last": "/blog/posts/json?user_id=2&page=2"
            },
            "page_num": 1,
            "num_pages": 2
        },
        "page_menu": {
            "context": "logical",
            "pages": {
                "1": {
                    "start": 0,
                    "url": null
                },
                "2": {
                    "start": 5,
                    "url": "/blog/posts/json?user_id=2&page=2"
                }
            },
            "page_num": 1,
            "num_pages": 2
        },
        "view_menu": {
            "context": "logical",
            "views": {
                "list": {
                    "display": "List",
                    "limit": 5,
                    "url": null
                },
                "grid": {
                    "display": "Grid",
                    "limit": 20,
                    "url": "/blog/posts/json?user_id=2&view=grid"
                },
                "item": {
                    "display": "Item",
                    "limit": 1,
                    "url": "/blog/posts/json?user_id=2&view=item"
                }
            },
            "selected": "list",
            "label_view_as": "View as"
        }
    },
    "keyword_search": {
        "config": {
            "scope": "query",
            "persist_sort": true,
            "persist_view": true,
            "persist_input": false
        },
        "route": "/blog/posts/json",
        "base_parameters": {
            "user_id": "2"
        },
        "keyword": "",
        "clear": "/blog/posts/json?user_id=2",
        "label_search": "Search"
    }
}
```

#### Get Comments by Post ID

##### Request

```text
/blog/comments/json?post_id=99
```

```json
{
    "query": {
        "self": "http://fresh58.srv/blog/comments/json?post_id=99",
        "query": "?post_id=99",
        "route": "/blog/comments/json",
        "parameters": {
            "inputs": {
                "post_id": "99"
            },
            "defaults": {
                "view": "list"
            },
            "all": {
                "post_id": "99",
                "view": "list"
            }
        },
        "view": "list",
        "limit": 5,
        "start": 0,
        "found": 5,
        "displayed": 5
    },
    "items": [
        {
            "id": 491,
            "post_id": 99,
            "name": "eos enim odio",
            "email": "Maxwell@adeline.me",
            "body": "natus commodi debitis cum ex rerum alias quis\nmaxime fugiat fugit sapiente distinctio nostrum tempora\npossimus quod vero itaque enim accusantium perferendis\nfugit ut eum labore accusantium voluptas",
            "list_view": "#",
            "grid_view": "/blog/comments/json?post_id=99&view=grid&start=0",
            "item_view": "/blog/comments/json?post_id=99&view=item&start=0",
            "default_view": "/blog/comments/json?post_id=99&start=0",
            "display_position": 1,
            "query_position": 0,
            "form_item_checked": ""
        },
        {
            "id": 492,
            "post_id": 99,
            "name": "consequatur alias ab fuga tenetur maiores modi",
            "email": "Amina@emmet.org",
            "body": "iure deleniti aut consequatur necessitatibus\nid atque voluptas mollitia\nvoluptates doloremque dolorem\nrepudiandae hic enim laboriosam consequatur velit minus",
            "list_view": "#",
            "grid_view": "/blog/comments/json?post_id=99&view=grid&start=1",
            "item_view": "/blog/comments/json?post_id=99&view=item&start=1",
            "default_view": "/blog/comments/json?post_id=99&start=1",
            "display_position": 2,
            "query_position": 1,
            "form_item_checked": ""
        },
        {
            "id": 493,
            "post_id": 99,
            "name": "ut praesentium sit eos rerum tempora",
            "email": "Gilda@jacques.org",
            "body": "est eos doloremque autem\nsimilique sint fuga atque voluptate est\nminus tempore quia asperiores aliquam et corporis voluptatem\nconsequatur et eum illo aut qui molestiae et amet",
            "list_view": "#",
            "grid_view": "/blog/comments/json?post_id=99&view=grid&start=2",
            "item_view": "/blog/comments/json?post_id=99&view=item&start=2",
            "default_view": "/blog/comments/json?post_id=99&start=2",
            "display_position": 3,
            "query_position": 2,
            "form_item_checked": ""
        },
        {
            "id": 494,
            "post_id": 99,
            "name": "molestias facere soluta mollitia totam dolorem commodi itaque",
            "email": "Kadin@walter.io",
            "body": "est illum quia alias ipsam minus\nut quod vero aut magni harum quis\nab minima voluptates nemo non sint quis\ndistinctio officia ea et maxime",
            "list_view": "#",
            "grid_view": "/blog/comments/json?post_id=99&view=grid&start=3",
            "item_view": "/blog/comments/json?post_id=99&view=item&start=3",
            "default_view": "/blog/comments/json?post_id=99&start=3",
            "display_position": 4,
            "query_position": 3,
            "form_item_checked": ""
        },
        {
            "id": 495,
            "post_id": 99,
            "name": "dolor ut ut aut molestiae esse et tempora numquam",
            "email": "Alice_Considine@daren.com",
            "body": "pariatur occaecati ea autem at quis et dolorem similique\npariatur ipsa hic et saepe itaque cumque repellendus vel\net quibusdam qui aut nemo et illo\nqui non quod officiis aspernatur qui optio",
            "list_view": "#",
            "grid_view": "/blog/comments/json?post_id=99&view=grid&start=4",
            "item_view": "/blog/comments/json?post_id=99&view=item&start=4",
            "default_view": "/blog/comments/json?post_id=99&start=4",
            "display_position": 5,
            "query_position": 4,
            "form_item_checked": ""
        }
    ],
    "pagination": {
        "config": {
            "pager": {
                "make": true,
                "context": "logical"
            },
            "page_menu": {
                "make": true,
                "context": "logical",
                "max_links": 5
            },
            "view_menu": {
                "make": true,
                "context": "logical"
            },
            "use_page_number": true
        },
        "route": "/blog/comments/json",
        "viewing": {
            "view": "list",
            "first": "1",
            "last": "5",
            "range": "1~5",
            "found": 5,
            "displayed": 5
        },
        "pager": {
            "context": "logical",
            "pages": {
                "first": null,
                "prev": null,
                "next": null,
                "last": null
            },
            "page_num": 1,
            "num_pages": 1
        },
        "page_menu": {
            "context": "logical",
            "pages": {
                "1": {
                    "start": 0,
                    "url": null
                }
            },
            "page_num": 1,
            "num_pages": 1
        },
        "view_menu": {
            "context": "logical",
            "views": {
                "list": {
                    "display": "List",
                    "limit": 5,
                    "url": null
                },
                "grid": {
                    "display": "Grid",
                    "limit": 20,
                    "url": "/blog/comments/json?post_id=99&view=grid"
                },
                "item": {
                    "display": "Item",
                    "limit": 1,
                    "url": "/blog/comments/json?post_id=99&view=item"
                }
            },
            "selected": "list",
            "label_view_as": "View as"
        }
    },
    "keyword_search": {
        "config": {
            "scope": "query",
            "persist_sort": true,
            "persist_view": true,
            "persist_input": false
        },
        "route": "/blog/comments/json",
        "base_parameters": {
            "post_id": "99"
        },
        "keyword": "",
        "clear": "/blog/comments/json?post_id=99",
        "label_search": "Search"
    }
}
```
